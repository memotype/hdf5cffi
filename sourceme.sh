#!/bin/bash

if [[ -z $HDF5_SOURCEME ]]; then
    export HDF5_SOURCME=1

    . virtualenvwrapper.sh
    workon hdf5

    export PYTHONPATH="$PYTHON_PATH:$PWD"
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$PWD/upstream-src/hdf5/src/.libs/:$PWD/upstream-src/hdf5/hl/src/.libs"
else
    echo "$0 already sourced"
fi
