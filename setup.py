#!/usr/bin/env python
# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

from distutils.core import setup
import hdf5

setup(name='hdf5cffi',
      version='0.0.1',
      description='A python HDF5 library using CFFI for PyPy compatibility.',
      license='Apache License 2.0'
      author='Isaac Freeman',
      author_email='memotype@gmail.com',
      url='https://bitbucket.org/memotype/hdf5cffi',
      packages=['hdf5', 'wrapper'],
      ext_modules=[hdf5.h5.h5_ffi.verifier.get_extension()],
      )
