#!/usr/bin/env python

import hdf5
from hdf5.h5 import *
from hdf5 import h5data, h5err, H5File

h5file = H5File('test.h5', 't')
dspace = h5data.DataSpace([4, 6])
dset = h5data.DataSet(h5file, '/dset', H5T_STD_I32BE, dspace)

dset.close()
dspace.close()
h5file.close()


