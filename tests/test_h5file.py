# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

import os
import shutil
from random import randint
from tempfile import mkstemp, mkdtemp

from pytest import *

from hdf5 import h5file
from hdf5.h5file import H5File
from hdf5 import h5err
from hdf5 import h5


######################
### Util functions ###
######################

def modes(*ms):
    ''' Shortcut for parametrizing the 'mode' arg '''
    return mark.parametrize('mode', ms)
amodes = modes('x', 't', 'r', 'w')
cmodes = modes('x', 't')
omodes = modes('r', 'w')

def assertCreated(f):
    assert f.id > 0
    assert os.path.exists(f.name)

def assertClosed(f):
    assert f.closed
    with raises(h5file.H5IOError):
        int(f)

def h5del(f):
    f.close
    if os.path.exists(f.name):
        os.remove(f.name)


################
### Fixtures ###
################

@fixture(scope='class')
def testfile(request):
    testdir = mkdtemp()
    request.addfinalizer(lambda: shutil.rmtree(testdir, True))

    testfile_path = str(os.path.join(testdir, 'test.h5'))
    testfile = H5File(testfile_path, 't')
    request.addfinalizer(lambda: h5del(testfile))

    return testfile

@fixture()
def tmp_path(request, tmpdir):
    request.addfinalizer(lambda: shutil.rmtree(str(tmpdir), True))
    return str(tmpdir.join('test.h5'))

@fixture()
def tmp_h5open(request, tmp_path):
    f = H5File(tmp_path, 'x')
    #request.addfinalizer(lambda: h5del(f))
    return f

@fixture()
def tmp_h5closed(request, tmp_path):
    f = H5File(tmp_path, 'x')
    f.close()
    #request.addfinalizer(lambda: h5del(f))
    return tmp_path

@fixture()
def tmp_nonh5(request, tmp_path):
    f = file(tmp_path, 'w')
    f.writelines(['Some filler'])
    f.flush()
    f.close()
    return tmp_path

#############
### Tests ###
#############

#def test_is_hdf5(tmp_h5closed):
#    assert h5file.H5File.is_hdf5(tmp_h5closed) > 0

#def test_is_not_hdf5(tmp_nonh5):
#    assert h5file.H5File.is_hdf5(tmp_nonh5) == 0

#def test_is_hdf5_nonexisting_fail(tmp_path):
#    with raises(h5.H5Error):
#        h5file.H5File.is_hdf5(tmp_path)

### File create/open tests ###

@cmodes
def test_create_new(tmp_path, mode):
    ''' Pre-test file creation '''
    f = H5File(tmp_path, mode)
    assertCreated(f)
    f.close()

@modes('t')
def test_open_trunc(tmp_nonh5, mode):
    f = H5File(tmp_nonh5, mode)
    assert os.path.getsize(tmp_nonh5) == 0
    f.close()

@omodes
def test_open_nonhdf5(tmp_nonh5, mode):
    with raises(h5file.H5IOError):
        f = H5File(tmp_nonh5, mode)

@modes('x')
def test_open_excl_existing_fail(tmp_h5closed, mode):
    with raises(h5.H5Error):
        f = H5File(tmp_h5closed, mode)

@omodes
def test_open_existing(tmp_h5closed, mode):
    f = H5File(tmp_h5closed, mode)
    f.close()

@omodes
def test_open_nonexisting_fail(tmp_path, mode):
    with raises(h5.H5Error):
        f = H5File(tmp_path, mode)

@omodes
def test_open_again_fail(tmp_h5open, mode):
    with raises(h5file.H5IOError):
        f = H5File(tmp_h5open.name, mode)

@cmodes
def test_open_again_create_fail(tmp_h5open, mode):
    with raises(h5.H5Error):
        f = H5File(tmp_h5open.name, mode)

def test_reopen(tmp_h5open):
    f = tmp_h5open.reopen()



### H5File pythonic helpers tests ###

def test_with(tmp_path):
    with H5File(tmp_path, 'x') as f:
        assertCreated(f)
    assertClosed(f)

# TODO: Test more things...
class TestFoo:
    def test_foo(self, testfile):
        print 'XXX', testfile.name, testfile.id
    def test_bar(self, testfile):
        print 'XXX', testfile.name, testfile.id

