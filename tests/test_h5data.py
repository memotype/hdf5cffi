# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

import os
import shutil
from random import randint
from tempfile import mkstemp, mkdtemp

from pytest import *

from hdf5 import h5file
from hdf5.h5file import H5File
from hdf5 import h5err
from hdf5 import h5
from hdf5 import h5data
from hdf5.h5data import *


######################
### Util functions ###
######################

def h5del(f):
    f.close
    if os.path.exists(f.name):
        os.remove(f.name)


################
### Fixtures ###
################

# TODO: Refactor in to utilities module?

@fixture(scope='class')
def testfile(request):
    testdir = mkdtemp()
    request.addfinalizer(lambda: shutil.rmtree(testdir, True))

    testfile_path = str(os.path.join(testdir, 'test.h5'))
    testfile = H5File(testfile_path, 't')
    request.addfinalizer(lambda: h5del(testfile))

    return testfile

@fixture()
def tmp_path(request, tmpdir):
    request.addfinalizer(lambda: shutil.rmtree(str(tmpdir), True))
    return str(tmpdir.join('test.h5'))

@fixture()
def dsempty():
    return DataSpace()

@fixture()
def dsones():
    return DataSpace([1, 1])

#############
### Tests ###
#############

def test_DataSpace_init(dsempty):
    assert hasattr(dsempty, 'id')
    assert dsempty.id > 0

def test_DataSpace_close(dsempty):
    dsempty.close()
    assert dsempty.closed

def test_with_DataSpace():
    with DataSpace() as ds:
        assert ds.id > 0
    assert ds.closed

def test_DataSpace_dims(dsempty):
    assert dsempty.ndims == 1
    res, dims, maxdims = dsempty.dims(2, 2)
    assert list(dims) == [0, 0]
    assert list(maxdims) == [0, 0]

def test_DataSpace_dims_ones(dsones):
    assert dsones.ndims == 2
    res, dims, maxdims = dsones.dims(2, 2)
    assert list(dims) == [1, 1]
    assert list(maxdims) == [1, 1]

def test_DataSpace_copy(dsempty):
    dscopy = dsempty.copy()
    assert dscopy.id != dsempty.id
    assert dscopy != dsempty

