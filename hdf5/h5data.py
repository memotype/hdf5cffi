# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

import cffi

from hdf5 import h5, h5err
from wrapper import WrapObj

__all__ = [
    'DataSpace',
    'DataSet',
]

class DataSpace(WrapObj):
    ''' An HDF5 dataspace
    
    TODO:
      * H5Sdecode/H5Sencode
      * normal/non-simple creation?
      * Seperate class for extents?
    '''

    _props = {
        'select_elem_npoints': h5.H5Sget_select_elem_npoints,
        'ndims': h5.H5Sget_simple_extent_ndims,
        'npoints': h5.H5Sget_simple_extent_npoints,
        'type': h5.H5Sget_simple_extent_type,
        'is_simple':h5.H5Sis_simple,
    }

    _meths = {
        'offset_simple': h5.H5Soffset_simple,
        'dims': (h5.H5Sget_simple_extent_dims, [], [], [1,2]),
        'h5close': h5.H5Sclose,
    }

    unlimited = h5.H5S_UNLIMITED

    def __init__(self, current_dims=[0], maximum_dims=None, copy=None):
        ''' Create a new dataspace, with dimensions current_dims and maximum
        dimensions (optional) maximum_dims.
        
        current_dims and maximum_dims are lists of ints and/or
        DataSpace.unlimited.

        Supports python's 'copy' module, or supply a DataSpace object as the
        'copy' keyword argument.

        '''

        WrapObj.__init__(self)

        self.closed = True

        if copy:
            self.rank = copy.rank
            self.id = h5.H5Scopy(copy)
        else:
            self.rank = len(current_dims)
            if maximum_dims == None:
                maximum_dims = cffi.FFI.NULL
            # TODO: non-simple?
            self.id = h5.H5Screate_simple(self.rank, current_dims, maximum_dims)

        if self.id < 0:
            raise h5err.H5DataError(
                    'Error creating DataSpace with current_dims {0} and ' +
                    'maximum_dims {1}: error code {2}'
                    .format(current_dims, maximum_dims, self.id))
        else:
            self.closed = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        self.h5close()
        self.closed = True

    def copy(self):
        return DataSpace(copy=self)
    __copy__ = copy

    def __int__(self):
        return self.id
    __trunc__ = __int__

    def __deepcopy__(self):
        # TODO: Does HDF5 do a "deep" copy by default? Does that even make
        # sense? Need to research and test.
        return self.copy()


class DataSet(WrapObj):
    ''' An HDF5 dataset '''

    _consts = {
        'd': h5.H5P_DEFAULT,
        # TODO...
    }
    _props = {
        'dcpl': h5.H5Dget_create_plist,
        'dapl': h5.H5Dget_access_plist,
        'offset': h5.H5Dget_offset,
        'space': h5.H5Dget_space,
        'space_status': h5.H5Dget_space_status, # TODO: this is special...
        'storage_size': h5.H5Dget_storage_size,
        'type': h5.H5Dget_type,
    }
    _meths = {
        'close': h5.H5Dclose,
        'read': (h5.H5Dread, {'arrays': [5]}),
        'write': h5.H5Dwrite,
    }

    def __init__(self, h5file, name, dtype, dspace, 
                 lcpl='d', dcpl='d', dapl='d'):
        
        WrapObj.__init__(self)

        self.h5file = h5file
        #self.name = name
        #self.dtype = dtype
        #self.dspace = dspace
        #self.lcpl = lcpl
        #self.dcpl = dcpl
        #self.dapl = dapl

        c_lcpl, c_dcpl, c_dapl = map(self._consts.get, [lcpl, dcpl, dapl]) 
        self.id = h5.H5Dcreate(h5file, name, dtype, dspace, 
                                      c_lcpl, c_dcpl, c_dapl)

        if self.id < 0:
            raise h5err.H5DataError('Error creating DataSet, error code: {0}'
                                    .format(self.id))
        
    def __int__(self):
        return self.id
    __trunc__ = __int__
