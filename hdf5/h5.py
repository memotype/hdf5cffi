# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

'''
TODO
'''

# XXX: Don't forget to `del` any imported modules to keep the namespace clean.
# This module is meant to be "import *"'d, so we don't want to dump modules in
# the user's namespace.
import os
from cffi import FFI
import wrapper

__all__ = ['NULL', 'h5_api']  # Appended to dynamically at the end


class H5Error(Exception): 
    ''' Generic, API-level exception. '''
    pass

### HDF5 FFI code, paths, etc ###

# TODO: Hack. More flexibility/customizability needed...
_modpath = os.path.dirname(__file__)
_cffi_code = file(os.path.join(_modpath, 'hdf5.cffi')).read()
_verify_code = '#include "hdf5.h"'

_include_dirs = [
    './upstream-src/hdf5/src',
]

_library_dirs = [
    './upstream-src/hdf5/src/.libs',
    './upstream-src/hdf5/hl/src/.libs',
]

_libraries = [
    'hdf5',
]


### Do the thing ###
h5_ffi = FFI()
h5_ffi.cdef(_cffi_code)
h5_api = h5_ffi.verify(_verify_code,
                       include_dirs=_include_dirs,
                       library_dirs=_library_dirs,
                       libraries=_libraries)


### Other/misc values ###
# XXX: Don't forget to add anything here to __all__ at the top.

NULL = h5_ffi.NULL


### Wrap and export h5_api's functions ###

class _H5Func(wrapper.CFunc):
    def checkerr(self, retval, args):
        ''' Checks for negative return values and raises H5Error. '''
        if retval < 0:
            raise H5Error(('An error occured calling {0} with args {1}. ' +
                           'Error code: {2}').format(self.name, args, retval))
        else:
            return retval

_cfuncs = _H5Func.fromAPI(h5_api, h5_ffi)
globals().update(_cfuncs)
__all__ += _cfuncs.keys()


### Everyone hates cluttered namespaces ###

del FFI
del os
#del cffi

