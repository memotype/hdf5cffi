# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

from hdf5 import h5, h5err
from wrapper import WrapObj

__all__ = [
    'H5File',
]


class H5IOError(h5.H5Error): pass


class H5File(WrapObj):
    ''' An HDF5 data file '''

    _meths = {
        'is_hdf5': staticmethod(h5.H5Fis_hdf5),
        '_open': staticmethod(h5.H5Fopen),
        '_create': staticmethod(h5.H5Fcreate),
        'reopen': h5.H5Freopen,
    }

    _modes = {
        'r': h5.H5F_ACC_RDONLY,
        'w': h5.H5F_ACC_RDWR,
        't': h5.H5F_ACC_TRUNC,
        'x': h5.H5F_ACC_EXCL,
    }
    _consts = {
        'd': h5.H5P_DEFAULT,
        # TODO: ...?
    }

    def __init__(self, name, mode='r', aprop='d', cprop='d'):
        '''
        Opens or creates an HDF5 file, raising H5IOError(IOError) if a
        problem is encountered. Arguments are similar to the built-in file().

        - name: The path to the file.
        - mode: 'r' or 'w' open existing files in read-only or read/write mode,
        respectively. 't' or 'x' creates a new file by truncating any existing
        file ('t') or raising an H5IOError if a file already exists ('x').
        - aprop: File access properties: 'd' for default, TODO: ...?
        - cprop: File creation properties, ignored if mode is 'r' or 'w': 'd'
        for default, TODO: ...?

        '''

        WrapObj.__init__(self)

        self.name = name
        self.mode = mode
        self.cprop = cprop
        self.aprop = aprop

        # In case something happens before opening.
        self.closed = True

        c_cprop, c_aprop = map(self._consts.get, [cprop, aprop])

        if mode in ['r', 'w']:
            if self.is_hdf5(name) > 0:
                #self.id = h5.H5Fopen(name, self._modes[mode], c_aprop)
                self.id = self._open(name, self._modes[mode], c_aprop)
            else:
                raise H5IOError('File {0} does not appear to be in HDF5 format'
                                .format(name))
        elif mode in ['t', 'x']:
            #self.id = h5.H5Fcreate(name, self._modes[mode], c_cprop,
            self.id = self._create(name, self._modes[mode], c_cprop, c_aprop)
        else:
            raise h5.H5Error('Invalid file mode, {0}'.format(mode))

        #self._checkerr(self.id)

        self.closed = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # TODO: Should we do something different based on the exc?
        self.close()

    def __copy__(self):
        # TODO
        # Actually, maybe shouldn't implement this. HDF5 doesn't like having
        # multiple references to the same file... Hmm.. maybe should just copy
        # the file to a new file and open it? would need a new file name. Or, 
        # maybe could copy to in-memory-only file?
        pass    

    def __int__(self):
        # CFFI calls this when the H5File object is being passed to a function
        # expecting an int.
        if not self.closed:
            return self.id
        else:
            raise H5IOError('Attempt to use H5File after it was closed.')
    def __trunc__(self):
        int(self)

    def close(self):
        ''' Closes the file

        h5file.closed will be set to True and all further operations will fail
        with H5IOError.

        H5File also supports 'with' contexts and will be closed normally when
        the context ends.

        '''

        if not self.closed:
            status = h5.H5Fclose(self.id)
            if status < 0:
                raise H5IOError('Error closing {0}: error code {1}'
                                      .format(self.name, status))
            else:
                self.closed = True

        return self.closed

    __del__ = close

    def _checkerr(self, retval, cfunc, args):
        if retval < 0:
            raise H5IOError('Error returned cfunc with args {0}. Retval {1}'
                            .format(args, retval))
        else:
            return retval

